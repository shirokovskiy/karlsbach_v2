/*$j(document).ready(function() {

		$j(window).scroll(function() {
    if ($j(window).scrollTop() > 5) {
        $j('.page-header').addClass('fixheader');

    }
    else{
        $j('.page-header').removeClass('fixheader');
    }
});
}); */





/*OWL*/
$j(document).ready(function() {

  $j("#owl-demo").owlCarousel({

      autoPlay: 3000, //Set AutoPlay to 3 seconds
	  navigation : true,
	  pagination : false,
      items : 1,
      itemsDesktop: false,
      itemsDesktopSmall : false,
	  itemsMobile: false,
	  itemsTablet: false,
	  itemsTabletSmall: false,
 	   animateOut: 'slideOutDown',
    animateIn: 'flipInX',
    stagePadding:30,
    smartSpeed:450
  });

});

/*
$j(document).ready(function() {

  var time = 7; // time in seconds

  var $jprogressBar,
      $jbar,
      $jelem,
      isPause,
      tick,
      percentTime;

    //Init the carousel
    $j("#owl-demo").owlCarousel({
      slideSpeed : 500,
      paginationSpeed : 500,
	  pagination: false,
	  navigation: true,
      singleItem : true,
      afterInit : progressBar,
      afterMove : moved,
      startDragging : pauseOnDragging
    });

    //Init progressBar where elem is $("#owl-demo")
    function progressBar(elem){
      $jelem = elem;
      //build progress bar elements
      buildProgressBar();
      //start counting
      start();
    }

    //create div#progressBar and div#bar then prepend to $("#owl-demo")
    function buildProgressBar(){
      $jprogressBar = $j("<div>",{
        id:"progressBar"
      });
      $jbar = $j("<div>",{
        id:"bar"
      });
      $jprogressBar.append($jbar).prependTo($jelem);
    }

    function start() {
      //reset timer
      percentTime = 0;
      isPause = false;
      //run interval every 0.01 second
      tick = setInterval(interval, 10);
    };

    function interval() {
      if(isPause === false){
        percentTime += 1 / time;
        $jbar.css({
           width: percentTime+"%"
         });
        //if percentTime is equal or greater than 100
        if(percentTime >= 100){
          //slide to next item
          $jelem.trigger('owl.next')
        }
      }
    }

    //pause while dragging
    function pauseOnDragging(){
      isPause = true;
    }

    //moved callback
    function moved(){
      //clear interval
      clearTimeout(tick);
      //start again
      start();
    }

    //uncomment this to make pause on mouseover
    // $elem.on('mouseover',function(){
    //   isPause = true;
    // })
    // $elem.on('mouseout',function(){
    //   isPause = false;
    // })

}); */

/*$j(document).ready(function() {

  var sync1 = $j("#sync1");
  var sync2 = $j("#sync2");

  sync1.owlCarousel({
    singleItem : true,
    slideSpeed : 1000,
    navigation: true,
    pagination:false,
    afterAction : syncPosition,
    responsiveRefreshRate : 200,
  });

  sync2.owlCarousel({
    items : 15,
    itemsDesktop      : [1199,10],
    itemsDesktopSmall     : [979,10],
    itemsTablet       : [768,8],
    itemsMobile       : [479,4],
    pagination:false,
    responsiveRefreshRate : 100,
    afterInit : function(el){
      el.find(".owl-item").eq(0).addClass("synced");
    }
  });

  function syncPosition(el){
    var current = this.currentItem;
    $j("#sync2")
      .find(".owl-item")
      .removeClass("synced")
      .eq(current)
      .addClass("synced")
    if($j("#sync2").data("owlCarousel") !== undefined){
      center(current)
    }
  }

  $j("#sync2").on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $j(this).data("owlItem");
    sync1.trigger("owl.goTo",number);
  });

  function center(number){
    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
    var num = number;
    var found = false;
    for(var i in sync2visible){
      if(num === sync2visible[i]){
        var found = true;
      }
    }

    if(found===false){
      if(num>sync2visible[sync2visible.length-1]){
        sync2.trigger("owl.goTo", num - sync2visible.length+2)
      }else{
        if(num - 1 === -1){
          num = 0;
        }
        sync2.trigger("owl.goTo", num);
      }
    } else if(num === sync2visible[sync2visible.length-1]){
      sync2.trigger("owl.goTo", sync2visible[1])
    } else if(num === sync2visible[0]){
      sync2.trigger("owl.goTo", num-1)
    }

  }

});*/

$j(document).ready(function() {

  $j("#owl-new").owlCarousel({

      autoPlay: 3000, //Set AutoPlay to 3 seconds
	  navigation : true,
	  pagination : false,
      items : 4,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]

  });

});

$j(document).ready(function() {

  $j("#owl-top-categories").owlCarousel({

      autoPlay: 3000, //Set AutoPlay to 3 seconds
	  navigation : true,
	  pagination : false,
      items : 4,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]

  });

});

$j(document).ready(function() {

  $j("#owl-brands").owlCarousel({

      autoPlay: 3000, //Set AutoPlay to 3 seconds
	  navigation : true,
	  pagination : false,
      items : 6,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]

  });

});

$j(document).ready(function() {

  $j("#owl-sidebar-banner").owlCarousel({

      autoPlay: 3000, //Set AutoPlay to 3 seconds
	  navigation : false,
	  pagination : true,
      items : 1,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]

  });

});

/*
$j(document).ready(function() {
	$j(".srch-ico").click(function(){
				if($j("#header-search").css("display")=="none"){
				$j("#header-search").slideDown();
			}
				else{
					$j("#header-search").slideUp();
				}
		});
});*/

$j(document).ready(function() {
	$j(".srch-ico").click(function(){
				if($j("#header-search").css("display")=="none"){
				$j("#header-search").addClass('displaysearch');
			//	$j(".icon-search-2h").addClass('srch-ico-hide');
			//	$j(".fa-times").addClass('srch-ico-show');
			}
				else{
					$j("#header-search").removeClass('displaysearch');
			//		$j(".icon-search-2").removeClass('srch-ico-hide');
			//	    $j(".fa-times").removeClass('srch-ico-show');
				}
		});


	$j(window).scroll(function() {
    if ($j(window).scrollTop() > 260) {
		  $j('.scroll-top').addClass('scrolled-link');

    }
    else{
		 $j('.scroll-top').removeClass('scrolled-link');

    }
});
});




$j(document).ready(function() {
	// $j(".homeslider-content").addClass('wow slideInUp');
	 $j(".block-subscribe").addClass('wow slideInUp');
	 $j(".feature-box").addClass('wow slideInUp');
	 $j(".brands_slider").addClass('wow slideInUp');
	 $j(".footer-contact-info").addClass('wow slideInUp');
	 $j(".footer .links").addClass('wow slideInUp');
	 $j(".b1").addClass('wow slideInUp');
	 $j(".b2").addClass('wow slideInUp');
	 $j(".b3").addClass('wow slideInUp');
	 $j(".b4").addClass('wow slideInUp');
	 $j('.b2').css('-webkit-animation-delay', '0.2s');
     $j('.b3').css('-webkit-animation-delay', '0.3s');
     $j('.b4').css('-webkit-animation-delay', '0.5s');
	 $j(".new-products-title").addClass('wow slideInUp');
	 $j(".products-grid").addClass('wow slideInUp');
	 $j(".homepage_top_categories").addClass('wow slideInUp');
	 $j(".products-grid").addClass('wow slideInUp');
	 $j(".category-image").addClass('wow bounceIn');
	 $j(".toolbar").addClass('wow slideInUp');
});



/*
$j(document).ready(function() {

		var pageHeader = $j('.page-header');
		var headerFixedClass = 'fixheader';

		function removeFixedClass(){
			pageHeader.removeClass(headerFixedClass);
		}

		function pageHeaderScroll( header ){
			if( $j(window).scrollTop() > 10 ){
				header.addClass(headerFixedClass);
			}
			else {
				removeFixedClass();
			}
		}


		function headerFix(){
			removeFixedClass();

			if( $j(window).width() >= 1025 ){ // for desktop
				pageHeaderScroll(pageHeader);
			}
			else { // for tablet, mobile
				removeFixedClass();
			}
		}
		$j(window).on('load scroll resize orientationchange', headerFix);

});
*/

$j(document).ready(function() {
	$j(".page-header-container .account-cart-wrapper a.skip-link.skip-account").qtip({
		content: {
			//text: $j(".page-header-container .account-cart-wrapper a.skip-link.skip-account > span.label").text()
            attr: 'data-tooltip'
		},
        position: {
            my: 'right center',  // Position my top left...
            at: 'left center', // at the bottom right of...
            target: $j(".page-header-container .account-cart-wrapper a.skip-link.skip-account") // my target
        },
        style: {
            classes: 'qTipOverStyle'
        }
	});
});
