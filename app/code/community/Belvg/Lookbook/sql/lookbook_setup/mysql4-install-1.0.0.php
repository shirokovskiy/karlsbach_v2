<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Lookbook
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2014 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

$installer = $this;
$installer->startSetup();
$entityTypes = array('catalog_product');
foreach ($entityTypes as $type) {
    $entityTypeId = $installer->getEntityTypeId($type);

    $installer->addAttribute($type, 'belvg_lookbook_imagepos', array(
        'group' => 'General',
        'type' => 'varchar',
        'label' => 'Lookbook Image Position',
        'input' => 'select',
        'source' => 'lookbook/source_imageposition',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible' => TRUE,
        'required' => FALSE,
        'user_defined' => FALSE,
        'default' => ''
    ));

    $installer->addAttribute($type, 'belvg_lookbook_image', array(
        'group' => 'Images',
        'type' => 'varchar',
        'frontend' => 'catalog/product_attribute_frontend_image',
        'label' => 'Lookbook Image',
        'input' => 'media_image',
        'source' => 'lookbook/source_imageposition',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible' => TRUE,
        'required' => FALSE,
        'user_defined' => FALSE,
        'apply_to' => 'grouped',
        'default' => ''
    ));

}

$installer->run("
CREATE TABLE IF NOT EXISTS {$this->getTable('belvg_lookbook_positions')} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL,
  `simple_id` int(10) unsigned NOT NULL,
  `pos_x` int(16) unsigned NOT NULL,
  `pos_y` int(16) unsigned NOT NULL,
  `map` TEXT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
");

$installer->endSetup();
