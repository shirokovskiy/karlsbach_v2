<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   Belvg
 * @package    Belvg_Lookbook
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2014 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Lookbook_Model_Positions extends Mage_Core_Model_Abstract
{
    
    /**
     * Init model
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('lookbook/positions');
        $this->setIdFieldName('id');
    }
 
    public function getPosX()
    {
        return parent::getPosX()/100000000;
    
    }
 
    public function getPosY()
    {
        return parent::getPosY()/100000000;
    
    }
 
}