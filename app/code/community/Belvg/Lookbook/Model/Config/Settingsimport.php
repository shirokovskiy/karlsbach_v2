<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   Belvg
 * @package    Belvg_Lookbook
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2014 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Lookbook_Model_Config_Settingsimport extends Mage_Adminhtml_Model_System_Config_Backend_File
{
    
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $fields = array(
            'settings' => array(
                'enabled',
                'tooltip',
                'mapster'
            ),
            'styles' => array(
                'marker_image',
                'marker_image_width',
                'marker_image_height',
                'marker_border_width',
                'marker_border_radius',
                'marker_border_color',
                'marker_text_padding',
                'marker_text_color',
                'marker_font',
                'marker_text_shadow',
                'bg_marker_opacity',
                'bg_marker_color',
                'tooltip_bg_image',
                'tooltip_bg_image_width',
                'tooltip_bg_image_height',
                'tooltip_position',
                'tooltip_hor_offset',
                'tooltip_text_color',
                'tooltip_font',
                'tooltip_text_shadow',
                'popup_background_color',
                'popup_duration',
                'popup_overlay_opacity',
                'popup_overlay_color',
                'popup_border_color',
                'popup_border_width',
                'popup_border_radius',
                'popup_padding',
                'popup_shadow',
                'popup_close',
                'popup_close_image',
                'popup_image_width',
                'popup_image_height',
                'popup_name',
                'popup_name_font_color',
                'popup_name_font',
                'popup_price',
                'popup_price_font_color',
                'popup_price_font',
                'mapster_fill_opacity',
                'mapster_fill_color',
                'mapster_stroke_color',
                'mapster_stroke_opacity',
                'mapster_stroke_width',
                'mapster_fade',
                'mapster_fade_duration'
            )
        );
        $filepath = Mage::getBaseDir('base') . '/' . $this->_getUploadDir();
        $imported_settings = json_decode(file_get_contents($filepath . DS . $this->getValue()));
        
        $stores = array(0);
        foreach (Mage::app()->getStores() as $store) {
            $stores[$store->getId()] = $store->getId();
        }
        
        foreach ($stores as $store) {
            $store_settings = Mage::getStoreConfig('lookbook', $store);
            $scope = ($store == 0) ? 'default' : 'stores';
            foreach ($fields as $group => $values) {
                if (array_key_exists($group, $store_settings) && !is_array($store_settings[$group])) {
                    $store_settings[$group] = array();
                }
                
                foreach ($values as $key) {
                    $value = '';
                    if (array_key_exists($group, $store_settings) && array_key_exists($key, $store_settings[$group])) {
                        $value = $store_settings[$group][$key];
                    }
                    
                    if (isset($imported_settings)) {
                        if (isset($imported_settings[$store]->$group) && isset($imported_settings[$store]->$group->$key)) {
                            Mage::getConfig()->saveConfig('lookbook/' . $group . '/' . $key, $imported_settings[$store]->$group->$key, $scope, $store);
                        }
                    }
                }
            }
        }
        
        $this->setValue('');
        if (is_dir($filepath . DS)) {
            array_map('unlink', glob($filepath . DS . '*'));
            rmdir($filepath);
        }
        
        return $this;
    }
    
}