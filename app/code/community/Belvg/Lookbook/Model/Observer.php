<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Lookbook
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2014 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Lookbook_Model_Observer
{

    const POS_MULT = 100000000;

    /**
     * Saving video data for current product
     */
    public function saveProduct(Varien_Event_Observer $observer)
    {   //print_r($observer->getEvent()->debug());die;
        $params = Mage::app()->getRequest()->getParam('associated_products');
        if ($params) {
            foreach ($params as $key=>$param) {
                $data = $param;
                $data['parent_id'] = Mage::app()->getRequest()->getParam('id');;
                $data['simple_id'] = $key;
                $item = Mage::getModel('lookbook/positions')->getCollection()
                            ->addFieldToFilter('parent_id', $data['parent_id'])
                            ->addFieldToFilter('simple_id', $key)->getLastItem();
                if ($id = $item->getId()) {
                    $data['id'] = $id;
                }
                $data['pos_x'] = $data['pos_x']*self::POS_MULT;//pow(10,8);
                $data['pos_y'] = $data['pos_y']*self::POS_MULT;//pow(10,8);
                Mage::getModel('lookbook/positions')->setData($data)->save();
            }
        }
        //die;
    }

    public function defineImagePosition(Varien_Event_Observer $observer)
    {
        $request = $observer->getEvent()->getAction()->getRequest();
        if ($request->getModuleName() == 'catalog'
        && $request->getControllerName() == 'product' && Mage::helper('lookbook')->isFrontendEnabled()) {
            $product = Mage::getModel('catalog/product')->load($request->getParam('id'));
            try {
                if ((string)Mage::helper('catalog/image')->init($product, 'belvg_lookbook_image')) {
                    $imagePosition = $this->getImagePosition($product);
                    if ($imagePosition == 'top') {
                        $handle = 'lookbook_top';
                    } else {
                        $handle = 'lookbook_left';
                    }

                    $layoutUpdate = $observer->getEvent()->getLayout()->getUpdate();
                    $layoutUpdate->addHandle($handle);
                    $layoutUpdate->addHandle('lookbook_main');
                }
            } catch (Exception $e) {
                /* there is no belvg_lookbook_image' for this product */
            }
        }
    }

    public function getImagePosition($product)
    {
        if ($position = $product->getBelvgLookbookImagepos()) {
            return $position;
        }

        return (string)Mage::getStoreConfig('lookbook/settings/img_position');
    }
    
}
