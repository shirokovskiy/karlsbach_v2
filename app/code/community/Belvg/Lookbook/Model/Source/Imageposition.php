<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Lookbook
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2014 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Lookbook_Model_Source_Imageposition extends Belvg_Lookbook_Model_Source_Abstract
{
    
    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
        $helper = Mage::helper('lookbook');
        $this->_options = array(
            array('value' => 'top' ,'label' => $helper->__('Top (Horizontal Image)')),
            array('value' => 'left' ,'label' => $helper->__('Left (Vertical Image)'))
        );
        
        return $this->_options;
    }
    
}