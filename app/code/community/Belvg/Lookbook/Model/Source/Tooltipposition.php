<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Lookbook
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2014 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Lookbook_Model_Source_Tooltipposition
{
    
    /**
     * Options for select
     * @return array
     */
    public function toOptionArray()
    {
        $helper = Mage::helper('lookbook');
        return array(array('value' => 'top' ,'label' => $helper->__('Top')),
                     array('value' => 'bottom' ,'label' => $helper->__('Bottom')));
    }
    
}