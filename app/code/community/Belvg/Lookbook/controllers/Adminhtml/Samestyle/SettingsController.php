<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   Belvg
 * @package    Belvg_Lookbook
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2014 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Lookbook_Adminhtml_lookbook_SettingsController extends Mage_Adminhtml_Controller_Action
{

    public function resetAction()
    {
        $model = Mage::getModel('core/config_data')->getCollection()->addFieldToFilter('path', array('like' => 'lookbook%'))->getData();
        print_r($model);
    }
    
	public function exportAction()
    {
        //List of settings fields that we don't want to import or export.
		$excluded_fields = array(
			'conversionpro_enabled',
			'export_enabled',
			'global_export',
			'cron_enabled',
			'reset_cache',
			'export_settings',
			'import_settings',
			'import_override'
		);
		
		$fileName   = 'belvg_lookbook_settings.txt';
        $content = array();
		
		Mage::app()->setCurrentStore(0);
		$default_settings = Mage::getStoreConfig('lookbook', 0);
		foreach ($default_settings as $group => $values) {
			$content[0][$group] = array();
			foreach ($values as $key => $value) {
				if (in_array($key, $excluded_fields)) {
					continue;
				}
				
				$content[0][$group][$key] = $value;
			}
		}
		
		foreach (Mage::app()->getStores() as $store) {
			Mage::app()->setCurrentStore($store->getId());
			$store_settings = Mage::getStoreConfig('lookbook', $store->getId());
			foreach ($store_settings as $group => $values) {
				$content[$store->getId()][$group] = array();
				foreach ($values as $key => $value) {
					if (in_array($key, $excluded_fields)) {
						continue;
					}
					
					if ($content[0][$group][$key] != $value) {
						$content[$store->getId()][$group][$key] = $value;
					}
				}
			}
		}
		
		$this->_sendUploadResponse($fileName, json_encode($content));
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', TRUE);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', TRUE);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
    
    
}