<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Lookbook
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Lookbook_Block_Adminhtml_Product extends  Mage_Adminhtml_Block_Template
implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('belvg/lookbook/product_tab_form.phtml');
        $this->product = Mage::registry('current_product');
    }
    
    public function getTabLabel()
    {
        return $this->__('LookBook');
    }
    
    public function getTabTitle()
    {
        return $this->__('LookBook');
    }
    
    public function canShowTab()
    {
        $product = Mage::registry('current_product');
        if ($product->getTypeId() != 'grouped') {
            return FALSE;
        }
        
        try {
            $img = $this->helper('catalog/image')->init($product, 'belvg_lookbook_image');
        } catch (Exception $e) {
            $img = FALSE;
        }
        
        return ($img)?TRUE:FALSE;
    }
    
    public function isHidden()
    {
        return FALSE;
    }
    
    public function getAssociatedProducts()
    {
        $positions = Mage::getModel('lookbook/positions')->getCollection()
                                                          ->addFieldToFilter('parent_id', $this->getProduct()->getEntityId());
        $result = $this->getProduct()->getTypeInstance(true)->getAssociatedProducts($this->getProduct());
        foreach ($result as $item) {
            $item->setPositions($this->getItemPositions($item->getEntityId()));
            //print_r($item->getEntityId() . ' - ');
        }
        
        return $result;
        print_r($result);die;
    }
    
    public function getItemPositions($item_id)
    {
        return Mage::getModel('lookbook/positions')->getCollection()
                                                          ->addFieldToFilter('parent_id', $this->getProduct()->getEntityId())
                                                          ->addFieldToFilter('simple_id', $item_id)
                                                          ->getLastItem();
    }
    
    public function getItemImage($item, $x = 54, $y = 54)
    {
        try {
            $img = (string)Mage::helper('catalog/image')->init($item, 'image')->resize($x, $y);
        } catch (Exception $e) {
            $img = FALSE;
        }
        
        return $img;
    }
    
}