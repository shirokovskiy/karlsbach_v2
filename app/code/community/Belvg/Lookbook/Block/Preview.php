<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Lookbook
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Lookbook_Block_Preview extends Mage_Catalog_Block_Product_View_Type_Grouped
{
    public function getMediaDir()
    {
        return 'lookbook';
    }

    public function getAssociatedProducts()
    {
        $positions = Mage::getModel('lookbook/positions')->getCollection()
                                                          ->addFieldToFilter('parent_id', $this->getProduct()->getEntityId());
        $result = $this->getProduct()->getTypeInstance(true)->getAssociatedProducts($this->getProduct());
        foreach ($result as $item) {
            $item->setPositions($this->getItemPositions($item->getEntityId()));
            //print_r($item->getEntityId() . ' - ');
        }
        
        return $result;
        print_r($result);die;
    }
    
    public function getItemPositions($item_id)
    {
        return Mage::getModel('lookbook/positions')->getCollection()
                                                          ->addFieldToFilter('parent_id', $this->getProduct()->getEntityId())
                                                          ->addFieldToFilter('simple_id', $item_id)
                                                          ->getLastItem();
    
    }
    
    public function getStyleConfig()
    {
        return Mage::getStoreConfig('lookbook/styles');
    }
    
    
    public function getMarkerHeight()
    {
        return (int)Mage::getStoreConfig('lookbook/styles/marker_image_height');
    }
    
    public function getMarkerWidth()
    {
        return (int)Mage::getStoreConfig('lookbook/styles/marker_image_width');
    }
    
    public function getMarkerImage()
    {
        if (Mage::getStoreConfigFlag('lookbook/styles/marker_image')) {
            $width = $this->getMarkerWidth();
            $height = $this->getMarkerHeight();
            $src = Mage::getStoreConfig('lookbook/styles/marker_image');
            //$width = Mage::getStoreConfig('lookbook/window/width');
            //$height = Mage::getStoreConfig('lookbook/window/height');
            return '<img src="' . Mage::getBaseUrl('media') . 'lookbook' . DS . $src . '" width="' . $width . '" height="' . $height . '"/>';
        } else {
            return FALSE;
        }
    }

    public function isCloseButtonEnabled()
    {
        return Mage::getStoreConfigFlag('lookbook/styles/popup_close');
    }
    
    public function getCloseImage()
    {
        if (Mage::getStoreConfigFlag('lookbook/styles/popup_close')) {
            $src = Mage::getStoreConfig('lookbook/styles/popup_close_image');
            //$width = Mage::getStoreConfig('lookbook/window/width');
            //$height = Mage::getStoreConfig('lookbook/window/height');
            return '<img src="' . Mage::getBaseUrl('media') . 'lookbook' . DS . $src . '" width="' . '32' . '" height="' . '32' . '"/>';
        } else {
            return FALSE;
        }
    }
    
    public function getTooltipImageUrl()
    {
        if (Mage::getStoreConfigFlag('lookbook/settings/tooltip')) {
            $width = $this->getTooltipWidth();
            $height = $this->getTooltipHeight();
            $src = Mage::getStoreConfig('lookbook/styles/tooltip_bg_image');
            return $this->getImageFullUrl($src, $width, $height);
        } else {
            return FALSE;
        }
    }
    
    public function getTooltipHeight()
    {
        return (int)Mage::getStoreConfig('lookbook/styles/tooltip_bg_image_height');
    }
    
    public function getTooltipWidth()
    {
        return (int)Mage::getStoreConfig('lookbook/styles/tooltip_bg_image_width');
    }
    
    /**
     * Return full url of file
     * Resize file if needed and return url of new file
     *
     * @param string $filename
     * @param int $width
     * @param int $height
     * @param bool $transparency
     * @param bool $ratio
     * @return string
     */
    public function getImageFullUrl($filename, $width=NULL, $height=NULL, $transparency=TRUE, $ratio=FALSE)
    {
        if ($filename) {
            if ($width === NULL && $height === NULL) {
                return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . $this->getMediaDir() . DS . $filename;
            } else {
                $path = Mage::getBaseDir('media') . DS . $this->getMediaDir() . DS . $filename;
                if (file_exists($path)) {
                    $imageObj = new Varien_Image($path);
                    $imageObj->constrainOnly(TRUE);
                    $imageObj->keepAspectRatio($ratio);
                    //$imageObj->backgroundColor(NULL);
                    $imageObj->keepFrame(FALSE);
                    $imageObj->keepTransparency($transparency);
                    $imageObj->resize($width, $height);
                    $new_path = Mage::getBaseDir('media') . DS . $this->getMediaDir() . DS . $width . 'x' . $height . DS . $filename;
                    
                    try {
                        $imageObj->save($new_path);
                    } catch (Exception $e) {
                        return 'resize failed';
                    }
                    
                    return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . $this->getMediaDir() . DS . $width . 'x' . $height . DS . $filename;
                } else {
                    return 'file not exist: ' . $path;
                }
            }
        } else {
            return FALSE;
        }
    }
    
    public function isFrontendEnabled()
    {
        return Mage::helper('lookbook')->isFrontendEnabled();
    }
    
    public function isMapsterEnabled()
    {
        return Mage::getStoreConfigFlag('lookbook/settings/mapster');
    }
    
    public function isTooltipEnabled()
    {
        return Mage::getStoreConfigFlag('lookbook/settings/tooltip');
    }
    
    public function getImagePosition()
    {
        if ($position = $this->getProduct()->getBelvgLookbookImagepos()) {
            return $position;
        }
    
        return (string)Mage::getStoreConfig('lookbook/settings/img_position');
    }
    
    function getAnimationSpeed()
    {
        return (int)Mage::getStoreConfig('lookbook/styles/marker_speed');
    }
    
    function getMarkerDescBg()
    {
        $color = $this->hex2RGB('#' . Mage::getStoreConfig('lookbook/styles/bg_marker_color'), TRUE);
        $opacity = Mage::getStoreConfig('lookbook/styles/bg_marker_opacity');
        return $color . ',' . $opacity;
    }
    
    function hex2RGB($hexStr, $returnAsString = FALSE, $seperator = ',')
    {
        $hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr);
        $rgbArray = array();
        if (strlen($hexStr) == 6) {
            $colorVal = hexdec($hexStr);
            $rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
            $rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
            $rgbArray['blue'] = 0xFF & $colorVal;
        } elseif (strlen($hexStr) == 3) {
            $rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
            $rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
            $rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
        } else {
            return false;
        }
        
        return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray;
    }
    
    public function getMarkerType()
    {
        return Mage::getStoreConfig('lookbook/settings/marker');
    }
    
    public function getCustomStyles()
    {
        return Mage::getStoreConfig('lookbook/advanced/customcss');
    }
    
    public function getDetailPageUrl($product)
    {
        $vis_model = Mage::getModel('catalog/product_visibility');
        if ($product->getVisibility() != $vis_model::VISIBILITY_NOT_VISIBLE) {
            return $product->getProductUrl();
        }
         
        return FALSE;
    }
    
}