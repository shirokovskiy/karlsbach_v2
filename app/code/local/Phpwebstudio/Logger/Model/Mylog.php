<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 2/6/13
 * Time         : 11:18 AM
 * Description  :
 */
class Phpwebstudio_Logger_Model_Mylog extends Mage_Core_Model_Abstract
{
    public function __construct() {}

    /**
     * Pass any variable (object|array|int.... etc.) and get in log-file debug info with selected depth of method's calls.
     * Warning: don't set it more then 3.
     * Attention: this method not really ready, but in this version can work.
     * Examples:
     * 		myLog( $this );
     * 		myLog( $this->getCollection(), 0, 2, false );
     * 		myLog( $product, 0, 3 );
     * 		myLog( $myArray );
     *
     * @param $debugObj - "object" for debug (any variable)
     * @param int $level - current depth (is necessary for iterations). Always keep it ZERO.
     * @param int $limit - depth of function's calls
     * @param bool $showOnlyGetMethods - only such as getAllItems, getData, etc. methods of object  (if this is object)
     * @param bool $showData - calls getData even if method declared with parameters
     * @param bool $showProperties - output all properties of object (if this is object)
     */
    public function parse($debugObj, $level = 0, $limit = 1, $showOnlyGetMethods = true, $showData = false, $showProperties = false){
        $tabs = '';
        for($x=0; $x<$level; $x++){
            $tabs .= "|\t";
        }

        #date_default_timezone_set('Europe/Moscow'); // see in app/Mage.php function isInstalled()

        $fp = fopen(BP.DS."var/log/myLog.".date("Y.m.d").".log", "a+");

        if ($level==0) {
            fwrite($fp, "\nmyLog Start at ".date('Y-m-d H:i:s')." at ".date_default_timezone_get().' |');
        }

        if ($level >= $limit && is_object($debugObj)) {
            fwrite($fp, "\t\t-- STOP diving by LIMIT = $limit --");
            return;
        }

        $level++;
        if (is_object($debugObj)) {
            $classNameOfObj = get_class($debugObj);
            fwrite($fp, "\n".$tabs.'LEVEL #'.$level."\n".$tabs.$classNameOfObj/*.' is '.gettype($debugObj)*/."\n");
            $ms = get_class_methods($debugObj);
            sort($ms);

            if (is_array($ms)){
                $reflector = new ReflectionClass($classNameOfObj);
//			$isAbstract = $reflector->isAbstract();
//			$Ms = $reflector->getMethods();
                foreach($ms as $m){
//				$m = $M->getName();
                    if ($showOnlyGetMethods && !preg_match('/^get/',$m)) {
                        continue;
                    }
                    fwrite($fp, $tabs.print_r($m, true));
                    $params = $reflector->getMethod($m)->getParameters();
                    if (is_array($params)){
                        $param_pieces = array();
                        foreach($params as $param){
                            $param_pieces[] = $param->name;
                        }
                        fwrite($fp, '('.implode(',', $param_pieces).')');
                    }

                    if(preg_match('/^get/',$m)){
//					if (!count($params) && !preg_match('/date/i', $m)) {
                        if (!count($params) || ($m=='getData' && $showData)) { // если нет параметров - вызовем, или если это getData и хотим её отображать. Ахтунг: иногда getData выдаёт слишком большой массив данных, на который не хватает RAM.
                            fwrite($fp, "\t~~~~~~~ call =>");
                            try {
                                $reflectionMethod = new ReflectionMethod($classNameOfObj, $m);
                                $res = $reflectionMethod->invoke($debugObj);
//							$res = $debugObj->$m();
                            } catch(Exception $e) {
                                fwrite($fp, "\n".$tabs."Exception during call $classNameOfObj->$m()\n".$e->getMessage());
                            }

                            if(!is_object($res)){ // если после вызова вернулся объект
                                fwrite($fp, " return\t");
                            }
                            try {
                                $this->parse( $res, $level );
                            } catch(Exception $e){
                                fwrite($fp, "\t\t\t\t\t\t Ooops! Exception\t".$e->getMessage());
                            }
                        }
                    }
                    fwrite($fp, "\n");
                }
                // Get object properties
                if ($showProperties){
                    $props = $reflector->getProperties();
                    if (is_array($props) && !empty($props)) {
                        fwrite($fp, $tabs."Properties\n");
                        foreach($props as $prop){
                            fwrite($fp, $tabs.'->'.$prop->getName());
                            if ($prop->isPublic()){
                                fwrite($fp, ' = '.$prop->getValue());
                            }
                            elseif ($prop->isPrivate()){
                                fwrite($fp, ' is PRIVATE');
                            }else{
                                fwrite($fp, ' is PROTECTED');
                            }
                            fwrite($fp, "\n");
                        }
                    }
                }
                fwrite($fp, $tabs." * \n***\n * ".($level==0?"\n":''));
            } else {
                fwrite($fp, print_r($ms, true)."\n\n");
            }
        } else {
            if ($level==0) fwrite($fp, "\t"."not object [".gettype($debugObj)."]\n");
            if (is_array($debugObj)) {
                foreach ($debugObj as $k => $v) {
                    if (is_object($v))
                        $debugObj[$k] = get_class($v);
                }
                fwrite($fp, preg_replace("/\n/", "\n".$tabs, print_r($debugObj, true)));
            } else {
                if ($level==0) fwrite($fp, $tabs.'Deep-In ob_start()' . "\n");

                ob_start();
                var_dump($debugObj);
                $vardump_out = ob_get_contents();
                ob_clean();

                $vardump_out = preg_replace("/\n$/", "", $vardump_out);

                fwrite($fp, $vardump_out."\n");
            }
        }
        fclose($fp);
    }
}
