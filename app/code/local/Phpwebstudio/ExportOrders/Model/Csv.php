<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 2/3/13
 * Time         : 7:08 PM
 * Description  : save file in CSV format about Pending Orders
 *
 */
class Phpwebstudio_ExportOrders_Model_Csv extends Mage_Core_Model_Abstract {

    public function saveFile($filepath, $store_id = 1) {
        // let's export Order's data into CSV file, and save it
		$orders = Mage::getResourceModel('sales/order_collection');
        $orders->addFieldToFilter('status', array('eq'=>"pending")); // processing - для оплаченых через Райф

//        $orders->addFieldToFilter('status', array('eq'=>"processing"));
//        $orders->addFieldToFilter('increment_id', array('eq'=> 100000084)); // проверка конкретного заказа

        if (is_array($store_id) && is_int($store_id[0])) {
            $orders->addFieldToFilter('store_id', array('in'=> $store_id));
        } elseif(is_int($store_id)) {
            $orders->addFieldToFilter('store_id', array('eq'=> $store_id));
        } else {
            $orders->addFieldToFilter('store_id', array('in'=> array(1,4)));
        }

        $csvFile = new Varien_File_Csv();
        $ordersData = array();

        // Titles
        $data['order_id'] = 'order_id';
        $data['total_price'] = 'total_price';
        $data['order_date'] = 'order_date';
        $data['customer_id'] = 'customer_id';
        $data['lastname'] = 'lastname';
        $data['firstname'] = 'firstname';
        $data['phone'] = 'phone';
        $data['email'] = 'email';
        $data['postcode'] = 'postcode';
        $data['city'] = 'city';
        $data['address'] = 'address';
        $data['products'] = 'products';
        $data['services'] = 'services';
        $data['store'] = 'store';
        $ordersData[] = $data;
        $_is_data = false;

        // Data
        foreach($orders->getItems() as $order) {
            $_is_data = true;

            $data = array();
            $data['order_id'] = $order->getRealOrderId();
            $data['total_price'] = number_format($order->getGrandTotal(), 2, '.', '');
            $data['order_date'] = date('Y,m,d,H,i,s', strtotime($order->getCreatedAt()." +4 hours"));
            $data['customer_id'] = ($order->getShippingAddress()->getCustomerId()>10320?$order->getShippingAddress()->getCustomerId():'');
            $data['lastname'] = $order->getShippingAddress()->getLastname();
            $data['firstname'] = $order->getShippingAddress()->getFirstname();
            $data['phone'] = $order->getShippingAddress()->getTelephone();
            $data['email'] = $order->getCustomerEmail();
            $data['postcode'] = $order->getShippingAddress()->getPostcode();
            $data['city'] = $order->getShippingAddress()->getData('city');
            $data['address'] = $order->getShippingAddress()->getStreetFull();

            $items = $order->getItemsCollection();
            $arrItems = array();
            foreach($items->getItems() as $item) {
                $article = Mage::getModel('catalog/product')->load($item->getProductId())->getData('article');
                $sPrice = $item->getSpecialPrice() > 0 ? $item->getSpecialPrice() : $item->getPrice();
                $arrItems[] = $article.'|'.$item->getSku().'|'.number_format($item->getPrice(), 2, '.', '').'|'.number_format($sPrice, 2, '.', '').'|'.ceil($item->getQtyOrdered()).'|'.(float)$item->getDiscountPercent();
//                $arrItems[] = $article.'|'.$item->getSku().'|'.number_format($item->getPrice(), 2, '.', '').'|'.ceil($item->getQtyOrdered()).'|30'; // Dmitry Amosov
            }
            $data['products'] = (!empty($arrItems)? implode(';',$arrItems) : '');
            $data['services'] = '';
            $data['store'] = Mage::app()->getStore($order->getStoreId())->getCode();

            $ordersData[] = $data;
        }

        if (!$_is_data) {
            return false;
        }
        $csvFile->saveData($filepath, $ordersData);

        return (file_exists($filepath) && filesize($filepath));
	}


    /**
     * To check orders from CSV file
     * and set proper status
     *
     * @param string $fileName
     */
    public function checkCompleteOrders($fileName = 'orders_ok.csv') {  // этот файл выгружается на FTP
        #
        $order = Mage::getModel("sales/order"); // тут был мой Райф
        $csvFile = new Varien_File_Csv();
        $local_file_path = BP.DS.'var'.DS.'import'.DS.$fileName; // этот файл появляется если выгружает 1С
        $data = null;

        /**
         * Проверить если в файле от 1С есть BOM
         */
        if(file_exists($local_file_path)){
            $bom = pack("CCC", 0xef, 0xbb, 0xbf);
            $str = file_get_contents($local_file_path);
            if (0 == strncmp($str, $bom, 3)) {
                Mage::getModel('logger/mylog')->parse("BOM detected - file is UTF-8");
                $str = substr($str, 3);
                file_put_contents($local_file_path, $str);
            }

            // Read data from file
            $data = $csvFile->getData($local_file_path);
        } else {
            Mage::getModel('logger/mylog')->parse('Файл не существует: '.$local_file_path);
        }

        // checkout data
        if (is_array($data) && !empty($data)) {
            foreach($data[0] as $orderId) {

                Mage::getModel('logger/mylog')->parse('Пытаюсь обработать orderID = '.$orderId);

                $order->loadByIncrementId(intval($orderId));

                if ($order->getId() && $order->getData('status') == 'processingX') {            // временно отключено - убрать X чтобы включить
                    Mage::getModel('logger/mylog')->parse($orderId.' order has been found');
                    // set Shipping
                    if($order->canShip()) {
                        $convertor = Mage::getModel('sales/convert_order');
                        $shipment = $convertor->toShipment($order);

                        foreach ($order->getAllItems() as $orderItem) {
                            if (!$orderItem->getQtyToShip()) {
                                continue;
                            }
                            if ($orderItem->getIsVirtual()) {
                                continue;
                            }
                            $item = $convertor->itemToShipmentItem($orderItem);
                            $qty = $orderItem->getQtyToShip();
                            $item->setQty($qty);
                            $shipment->addItem($item);
                        }


                        /**
                         * Для того чтобы добавить метку о доставке, чтобы можно было отслеживать путь доставки
                         */
                        $data = array();
//                        $data['carrier_code'] = 'custom'; // $order->getShippingMethod();
                        $data['carrier_code'] = $order->getShippingMethod();
                        $data['title'] = $order->getShippingDescription();
                        $data['number'] = $order->getProtectCode();
                        $track = Mage::getModel('sales/order_shipment_track')->addData($data);
                        $shipment->addTrack($track);
                        $shipment->register();

                        /**
                         * Транзакционное сохранение одновременно и Доставки и Ордера
                         * Мне наверное не нужно здесь
                         */
                        /*$transactionSave = Mage::getModel('core/resource_transaction')
                            ->addObject($shipment)
                            ->addObject($shipment->getOrder())
                            ->save();*/

                        $shipment->sendEmail(true, '');
                        $shipment->setEmailSent(true);
                        $shipment->save();
                    }
                    // set Complete
                    $order->setState(Mage_Sales_Model_Order::STATE_COMPLETE, true, 'Товар отгружен покупателю.');

                    /**
                     * Альтернативный вариант, который не требует использования переписанного _setState() метода
                     */
//                    $order->setStatus('Complete');
//                    $order->addStatusToHistory($order->getStatus(), 'Товар отгружен покупателю.', false);

                    $order->save();
                }

                if ($order->getId() && $order->getData('status') == 'pending') {
                    Mage::getModel('logger/mylog')->parse('Ставлю заказ в режим Processing');

                    // Payment was successful, so update the order's state, send order email and move to the success page
                    $order = Mage::getModel('sales/order');
                    $order->loadByIncrementId($orderId);
                    $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true, 'Клиенту отправлен счёт.');
//                    $order->sendNewOrderEmail(); // 12/11/2013 - отключено по причине двойных писем-счетов
                    $order->setEmailSent(true);
                    $order->save();

                    // Тут же выписать инвойс и отправить email
                    try {
                        if (!$order->canInvoice()) {
                            Mage::throwException(Mage::helper('core')->__('Cannot create an invoice.'));
                        }

                        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();

                        if (!$invoice->getTotalQty()) {
                            Mage::throwException(Mage::helper('core')->__('Cannot create an invoice without products.'));
                        }

                        $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::NOT_CAPTURE); //CAPTURE_ONLINE);
                        $invoice->register();
                        //$invoice->sendEmail(); /* Send invoice email */
                        $invoice->setEmailSent(true); /* Set status to "the invoice email is sent" */

                        $transactionSave = Mage::getModel('core/resource_transaction')
                            ->addObject($invoice)
                            ->addObject($invoice->getOrder());
                        $transactionSave->save();

                        $order->addStatusToHistory($order->getStatus(), 'Ждём оплату.');
                    } catch (Mage_Core_Exception $e) {}
                }
            }

            if (unlink($local_file_path)===TRUE) {
                Mage::getModel('logger/mylog')->parse("Файл $local_file_path успешно удалён!");
            } else {
                Mage::getModel('logger/mylog')->parse("Немогу удалить файл $local_file_path");
            }
        }
    }
}
