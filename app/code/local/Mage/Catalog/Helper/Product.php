<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2018 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog category helper
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Catalog_Helper_Product extends Mage_Core_Helper_Url
{
    const XML_PATH_PRODUCT_URL_SUFFIX           = 'catalog/seo/product_url_suffix';
    const XML_PATH_PRODUCT_URL_USE_CATEGORY     = 'catalog/seo/product_use_categories';
    const XML_PATH_USE_PRODUCT_CANONICAL_TAG    = 'catalog/seo/product_canonical_tag';

    const DEFAULT_QTY                           = 1;

    /**
     * Flag that shows if Magento has to check product to be saleable (enabled and/or inStock)
     *
     * @var boolean
     */
    protected $_skipSaleableCheck = false;

    /**
     * Cache for product rewrite suffix
     *
     * @var array
     */
    protected $_productUrlSuffix = array();

    protected $_statuses;

    protected $_priceBlock;

    /**
     * Retrieve product view page url
     *
     * @param   mixed $product
     * @return  string
     */
    public function getProductUrl($product)
    {
        if ($product instanceof Mage_Catalog_Model_Product) {
            return $product->getProductUrl();
        }
        elseif (is_numeric($product)) {
            return Mage::getModel('catalog/product')->load($product)->getProductUrl();
        }
        return false;
    }

    public function getPromoLabel($product)
    {
        $allPromoSku = ["00440","00441","00442","00443","00444","00446","00447","00448","00449","00451","00452","00453","00655","00656","00657","00737","00739","00757","00758","00762","00763","00808","00821","00822","00849","00850","00851","00922","00924","00928","00929","00931","00934","00936","01070","01124","01266","01282","01287","01333","01334","01338","01352","01353","01354","01355","01356","01377","01378","01487","01488","01522","01525","01526","01529","01535","01542","01544","01546","01548","01556","01559","01560","01562","01568","01573","01575","01579","01580","01583","01959","01960","01978","01997","02008","02027","02028","02029","02030","02031","02032","02034","02038","02045","02046","02058","02071","02072","02073","02079","02091","02136","02137","02138","02313","02389","02431","02488","02524","02526","02531","02534","02538","02541","02542","02543","02545","02562","02566","02637","02638","02639","02640","02834","02841","02893","02948","02962","02988","02990","03136","03146","03164","03165","03166","03178","03200","03201","03202","03203","03298","03299","03303","03306","03312","03315","03316","03318","03322","03327","03330","03333","03495","03682","03683","03736","03754","03817","03819","03821","03822","03829","03830","03831","03832","03833","03834","03835","03836","03837","03838","04185","04210","04215","04216","04217","04226","04249","04253","04257","04258","04264","04266","04271","04272","04274","04277","04278","04279","04280","04288","04289","04302","04443","04522","04523","04524","04525","04597","04646","04647","04651","04652","04653","04654","04655","04657","04666","04673","04674","04676","04678","04679","04691","04701","04702","04703","04704","04712","04713","04721","04730","04734","04745","04750","04794","04806","04807","04809","04810","04837","04838","04841","04843","04898","04902","04931","04950","04951","05010","05011","05172","05340","05357","05362","05363","05365","05366","05367","05458","05459","05825","05875","05911","05979","05980","06046","06195","06481","06483","06485","06491","06495","06501","06505","06510","06511","06512","06520","06525","06526","06527","06528","06529","06530","06531","06532","06547","06548","06551","06553","06556","06560","06561","06569","06570","06571","06572","06579","06580","06581","06582","06583","06584","06599","06601","06604","06626","06627","06632","06633","06635","06643","06648","06657","06663","06666","06667","06687","06692","06693","06719","06721","06746","06751","06752","06780","06782","06789","06791","06792","06793","06795","06797","06798","06827","06831","06849","06850","06863","06864","06868","06872","06879","06880","06899","06902","06903","06904","06905","06906","06907","07000","07001","07002","07003","07004","07005","07017","07018","07020","07021","07028","07029","07030","07033","07034","07035","07037","07038","07041","07042","07043","07044","07048","07050","07051","07058","07061","07062","07063","07066","07067","07068","07070","07081","07082","07083","07087","07092","07093","07095","07096","07099","07100","07102","07110","07111","07112","07113","07114","07115","07116","07117","07118","07119","07120","07121","07122","07123","07124","07125","07126","07127","07128","07129","07130","07131","07213","07225","07227","07236","07362","07363","07382","07383","07384","07385","07386","07387","07388","07389","07392","07394","07397","07400","07406","07407","07408","07409","07627","07628","07629","07630","07636","07653","07654","07658","07660","07662","07672","07673","07676","07677","07678","07690","07691","07692","07695","07700","07703","07704","07706","07708","07709","07710","07711","07712","07713","07715","07716","07717","07718","07719","07720","07746","07747","07748","07749","07781","07784","07785","07786","07787","07788","07789","07791","07792","07793","07794","07795","08578","08580","08667","08668","08675","08676","08685","08686","08687","08688","08759","08775","08781","08783","08784","08785","08786","08822","08824","08827","08832","08834","08838","08844","08847","08848","08849","08851","08852","08853","08854","08855","08856","08857","08858","08859","08865","08868","08869","08871","08969","08970","08971","08972","08973","08974","08975","09020","09021","09022","09023","09024","09025","09026","09027","09029","09042","09043","09044","09045","09059","09061","09063","09089","09091","09093","09096","09114","09117","09118","09161","09162","09163","09164","09166","09167","09168","09169","09253","09254","09255","09256","09257","09258","09259","09260","09261","09262","09263","09264","09266","09272","09273","09274","09276","09277","09278","09280","09281","09283","09290","09291","09292","09293","09294","09295","09296","09297","09298","09299","09300","09301","09302","09303","09304","09305","09306","09307","09308","09309","09310","09311","09312","09313","09314","09315","09316","09320","09326","09327","09332","09333","09334","09335","09337","09338","09339","09340","09341","09342","09355","09366","09369","09402","09403","09404","09405","09407","09408","09409","09410","09412","09414","09415","09416","09417","09418","09419","09420","09421","09422","09423","09424","09425","09426","09427","09428","09429","09430"];
        foreach($allPromoSku as $promo_sku) {
            if ($product->getSku() == 'erp_1C_s4_'.$promo_sku.'-p') {
                return true;
            }
        }

        return false;
    }

    /**
     * Retrieve product view page url including provided category Id
     *
     * @param   int $productId
     * @param   int $categoryId
     * @return  string
     */
    public function getFullProductUrl($productId, $categoryId = null)
    {
        $product = Mage::getModel('catalog/product')->load($productId);
        if ($categoryId && $product->canBeShowInCategory($categoryId)) {
            $category = Mage::getModel('catalog/category')->load($categoryId);
            $product->setCategory($category);
        }
        return $product->getProductUrl();
    }

    /**
     * Retrieve product price
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  float
     */
    public function getPrice($product)
    {
        return $product->getPrice();
    }

    /**
     * Retrieve product final price
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  float
     */
    public function getFinalPrice($product)
    {
        return $product->getFinalPrice();
    }

    /**
     * Retrieve base image url
     *
     * @return string
     */
    public function getImageUrl($product)
    {
        $url = false;
        if (!$product->getImage()) {
            $url = Mage::getDesign()->getSkinUrl('images/no_image.jpg');
        }
        elseif ($attribute = $product->getResource()->getAttribute('image')) {
            $url = $attribute->getFrontend()->getUrl($product);
        }
        return $url;
    }

    /**
     * Retrieve small image url
     *
     * @return unknown
     */
    public function getSmallImageUrl($product)
    {
        $url = false;
        if (!$product->getSmallImage()) {
            $url = Mage::getDesign()->getSkinUrl('images/no_image.jpg');
        }
        elseif ($attribute = $product->getResource()->getAttribute('small_image')) {
            $url = $attribute->getFrontend()->getUrl($product);
        }
        return $url;
    }

    /**
     * Retrieve thumbnail image url
     *
     * @return unknown
     */
    public function getThumbnailUrl($product)
    {
        return '';
    }

    public function getEmailToFriendUrl($product)
    {
        $categoryId = null;
        if ($category = Mage::registry('current_category')) {
            $categoryId = $category->getId();
        }
        return $this->_getUrl('sendfriend/product/send', array(
            'id' => $product->getId(),
            'cat_id' => $categoryId
        ));
    }

    public function getStatuses()
    {
        if(is_null($this->_statuses)) {
            $this->_statuses = array();//Mage::getModel('catalog/product_status')->getResourceCollection()->load();
        }

        return $this->_statuses;
    }

    /**
     * Check if a product can be shown
     *
     * @param  Mage_Catalog_Model_Product|int $product
     * @return boolean
     */
    public function canShow($product, $where = 'catalog')
    {
        if (is_int($product)) {
            $product = Mage::getModel('catalog/product')->load($product);
        }

        /* @var $product Mage_Catalog_Model_Product */

        if (!$product->getId()) {
            return false;
        }

        return $product->isVisibleInCatalog() && $product->isVisibleInSiteVisibility();
    }

    /**
     * Retrieve product rewrite sufix for store
     *
     * @param int $storeId
     * @return string
     */
    public function getProductUrlSuffix($storeId = null)
    {
        if (is_null($storeId)) {
            $storeId = Mage::app()->getStore()->getId();
        }

        if (!isset($this->_productUrlSuffix[$storeId])) {
            $this->_productUrlSuffix[$storeId] = Mage::getStoreConfig(self::XML_PATH_PRODUCT_URL_SUFFIX, $storeId);
        }
        return $this->_productUrlSuffix[$storeId];
    }

    /**
     * Check if <link rel="canonical"> can be used for product
     *
     * @param $store
     * @return bool
     */
    public function canUseCanonicalTag($store = null)
    {
        return Mage::getStoreConfig(self::XML_PATH_USE_PRODUCT_CANONICAL_TAG, $store);
    }

    /**
     * Return information array of product attribute input types
     * Only a small number of settings returned, so we won't break anything in current dataflow
     * As soon as development process goes on we need to add there all possible settings
     *
     * @param string $inputType
     * @return array
     */
    public function getAttributeInputTypes($inputType = null)
    {
        /**
        * @todo specify there all relations for properties depending on input type
        */
        $inputTypes = array(
            'multiselect'   => array(
                'backend_model'     => 'eav/entity_attribute_backend_array'
            ),
            'boolean'       => array(
                'source_model'      => 'eav/entity_attribute_source_boolean'
            )
        );

        if (is_null($inputType)) {
            return $inputTypes;
        } else if (isset($inputTypes[$inputType])) {
            return $inputTypes[$inputType];
        }
        return array();
    }

    /**
     * Return default attribute backend model by input type
     *
     * @param string $inputType
     * @return string|null
     */
    public function getAttributeBackendModelByInputType($inputType)
    {
        $inputTypes = $this->getAttributeInputTypes();
        if (!empty($inputTypes[$inputType]['backend_model'])) {
            return $inputTypes[$inputType]['backend_model'];
        }
        return null;
    }

    /**
     * Return default attribute source model by input type
     *
     * @param string $inputType
     * @return string|null
     */
    public function getAttributeSourceModelByInputType($inputType)
    {
        $inputTypes = $this->getAttributeInputTypes();
        if (!empty($inputTypes[$inputType]['source_model'])) {
            return $inputTypes[$inputType]['source_model'];
        }
        return null;
    }

    /**
     * Inits product to be used for product controller actions and layouts
     * $params can have following data:
     *   'category_id' - id of category to check and append to product as current.
     *     If empty (except FALSE) - will be guessed (e.g. from last visited) to load as current.
     *
     * @param int $productId
     * @param Mage_Core_Controller_Front_Action $controller
     * @param Varien_Object $params
     *
     * @return false|Mage_Catalog_Model_Product
     */
    public function initProduct($productId, $controller, $params = null)
    {
        // Prepare data for routine
        if (!$params) {
            $params = new Varien_Object();
        }

        // Init and load product
        Mage::dispatchEvent('catalog_controller_product_init_before', array(
            'controller_action' => $controller,
            'params' => $params,
        ));

        if (!$productId) {
            return false;
        }

        $product = Mage::getModel('catalog/product')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($productId);

        if (!$this->canShow($product)) {
            return false;
        }
        if (!in_array(Mage::app()->getStore()->getWebsiteId(), $product->getWebsiteIds())) {
            return false;
        }

        // Load product current category
        $categoryId = $params->getCategoryId();
        if (!$categoryId && ($categoryId !== false)) {
            $lastId = Mage::getSingleton('catalog/session')->getLastVisitedCategoryId();
            if ($product->canBeShowInCategory($lastId)) {
                $categoryId = $lastId;
            }
        } elseif (!$product->canBeShowInCategory($categoryId)) {
            $categoryId = null;
        }

        if ($categoryId) {
            $category = Mage::getModel('catalog/category')->load($categoryId);
            $product->setCategory($category);
            Mage::register('current_category', $category);
        }

        // Register current data and dispatch final events
        Mage::register('current_product', $product);
        Mage::register('product', $product);

        try {
            Mage::dispatchEvent('catalog_controller_product_init', array('product' => $product));
            Mage::dispatchEvent('catalog_controller_product_init_after',
                            array('product' => $product,
                                'controller_action' => $controller
                            )
            );
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            return false;
        }

        return $product;
    }

    /**
     * Prepares product options by buyRequest: retrieves values and assigns them as default.
     * Also parses and adds product management related values - e.g. qty
     *
     * @param  Mage_Catalog_Model_Product $product
     * @param  Varien_Object $buyRequest
     * @return Mage_Catalog_Helper_Product
     */
    public function prepareProductOptions($product, $buyRequest)
    {
        $optionValues = $product->processBuyRequest($buyRequest);
        $optionValues->setQty($buyRequest->getQty());
        $product->setPreconfiguredValues($optionValues);

        return $this;
    }

    /**
     * Process $buyRequest and sets its options before saving configuration to some product item.
     * This method is used to attach additional parameters to processed buyRequest.
     *
     * $params holds parameters of what operation must be performed:
     * - 'current_config', Varien_Object or array - current buyRequest that configures product in this item,
     *   used to restore currently attached files
     * - 'files_prefix': string[a-z0-9_] - prefix that was added at frontend to names of file inputs,
     *   so they won't intersect with other submitted options
     *
     * @param Varien_Object|array $buyRequest
     * @param Varien_Object|array $params
     * @return Varien_Object
     */
    public function addParamsToBuyRequest($buyRequest, $params)
    {
        if (is_array($buyRequest)) {
            $buyRequest = new Varien_Object($buyRequest);
        }
        if (is_array($params)) {
            $params = new Varien_Object($params);
        }


        // Ensure that currentConfig goes as Varien_Object - for easier work with it later
        $currentConfig = $params->getCurrentConfig();
        if ($currentConfig) {
            if (is_array($currentConfig)) {
                $params->setCurrentConfig(new Varien_Object($currentConfig));
            } else if (!($currentConfig instanceof Varien_Object)) {
                $params->unsCurrentConfig();
            }
        }

        /*
         * Notice that '_processing_params' must always be object to protect processing forged requests
         * where '_processing_params' comes in $buyRequest as array from user input
         */
        $processingParams = $buyRequest->getData('_processing_params');
        if (!$processingParams || !($processingParams instanceof Varien_Object)) {
            $processingParams = new Varien_Object();
            $buyRequest->setData('_processing_params', $processingParams);
        }
        $processingParams->addData($params->getData());

        return $buyRequest;
    }

    /**
     * Return loaded product instance
     *
     * @param  int|string $productId (SKU or ID)
     * @param  int $store
     * @param  string $identifierType
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct($productId, $store, $identifierType = null)
    {
        /** @var $product Mage_Catalog_Model_Product */
        $product = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore($store)->getId());

        $expectedIdType = false;
        if ($identifierType === null) {
            if (is_string($productId) && !preg_match("/^[+-]?[1-9][0-9]*$|^0$/", $productId)) {
                $expectedIdType = 'sku';
            }
        }

        if ($identifierType == 'sku' || $expectedIdType == 'sku') {
            $idBySku = $product->getIdBySku($productId);
            if ($idBySku) {
                $productId = $idBySku;
            } else if ($identifierType == 'sku') {
                // Return empty product because it was not found by originally specified SKU identifier
                return $product;
            }
        }

        if ($productId && is_numeric($productId)) {
            $product->load((int) $productId);
        }

        return $product;
    }

    /**
     * Set flag that shows if Magento has to check product to be saleable (enabled and/or inStock)
     *
     * For instance, during order creation in the backend admin has ability to add any products to order
     *
     * @param bool $skipSaleableCheck
     * @return Mage_Catalog_Helper_Product
     */
    public function setSkipSaleableCheck($skipSaleableCheck = false)
    {
        $this->_skipSaleableCheck = $skipSaleableCheck;
        return $this;
    }

    /**
     * Get flag that shows if Magento has to check product to be saleable (enabled and/or inStock)
     *
     * @return boolean
     */
    public function getSkipSaleableCheck()
    {
        return $this->_skipSaleableCheck;
    }

    /**
     * Gets minimal sales quantity
     *
     * @param Mage_Catalog_Model_Product $product
     * @return int|null
     */
    public function getMinimalQty($product)
    {
        $stockItem = $product->getStockItem();
        if ($stockItem && $stockItem->getMinSaleQty()) {
            return $stockItem->getMinSaleQty() * 1;
        }
        return null;
    }

    /**
     * Get default qty - either as preconfigured, or as 1.
     * Also restricts it by minimal qty.
     *
     * @param Mage_Catalog_Model_Product $product
     * @return int|float
     */
    public function getDefaultQty($product)
    {
        $qty = $this->getMinimalQty($product);
        $configQty = $product->getPreconfiguredValues()->getQty();

        if ($product->isConfigurable() || $configQty > $qty) {
            $qty = $configQty;
        }

        if (is_null($qty)) {
            $qty = self::DEFAULT_QTY;
        }

        return $qty;
    }
}
