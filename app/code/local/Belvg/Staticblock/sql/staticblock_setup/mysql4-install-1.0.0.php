<?php

$installer = $this;
$installer->startSetup();

$blocks = Mage::getModel('cms/block');
$cms    = Mage::getModel('cms/page')->load('home', 'identifier');

$cms->setRootTemplate('one_column')->save();

$stores = array(0);

$data_blocks = array(
    'title' => 'Home Slider',
    'identifier' => 'homepage_slider',
    'stores' => $stores,
    'is_active' => 1,
    'content'	=> <<<EOB
		<div id="owl-demo" class="owl-carousel owl-theme">
<div class="item">
<div class="homeslider-content">
<div class="small-title">Get 50% off all items</div>
<div class="slider-caption">OUR STYLISH SHOP</div>
<div class="slider-description">In augue urna, nunc, tincidunt, augue, augue facilisis facilisis.</div>
<a class="slider-button" href="#"><span>Shop Now</span></a></div>
<img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/slide-img1_1.jpg" /></div>
<div class="item">
<div class="homeslider-content">
<div class="small-title">Get 50% off all items</div>
<div class="slider-caption">ITALY INSPIRED</div>
<div class="slider-description">In augue urna, nunc, tincidunt, augue, augue facilisis facilisis.</div>
<a class="slider-button" href="#"><span>Shop Now</span></a></div>
<img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/slide-img3.jpg" /></div>
<div class="item">
<div class="homeslider-content">
<div class="small-title">Get 50% off all items</div>
<div class="slider-caption">DESIGN HANDBAGS</div>
<div class="slider-description">In augue urna, nunc, tincidunt, augue, augue facilisis facilisis.</div>
<a class="slider-button" href="#"><span>Shop Now</span></a></div>
<img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/slide-img4.jpg" /></div>
</div>
EOB
);

$blocks->setData($data_blocks)->save();

$data_blocks = array(
	'title'      => 'Homepage Top Categories',
	'identifier' => 'homepage_top_categories',
	'stores'     => $stores,
	'is_active'  => 1,
	'content'    => <<<EOB
			<h2><span>Top Categories</span></h2>
<div id="owl-top-categories">
<div class="item">
<div class="content">Women</div>
<a href="#"><img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/banners/necklace2.jpg" /></a></div>
<div class="item">
<div class="content">Men</div>
<a href="#"><img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/shirts.jpg" /></a></div>
<div class="item">
<div class="content">Electronics</div>
<a href="#"><img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/camera.jpg" /></a></div>
<div class="item">
<div class="content">Furniture</div>
<a href="#"><img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/chair.jpg" /></a></div>
<div class="item">
<div class="content">Kids</div>
<a href="#"><img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/banners/kids3_1.jpg" /></a></div>
</div>
EOB
);

$blocks->setData($data_blocks)->save();

$data_blocks = array(
	'title'      => 'Content Banner',
	'identifier' => 'content_banner',
	'stores'     => $stores,
	'is_active'  => 1,
	'content'    => <<<EOB
			<div class="content_banners">
<div class="left-banners">
<div class="banner-wrapper b1">
<h3>Shoes</h3>
<a href="#">
<img alt="" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/banners/offer-banner11_1.jpg" /></a></div>
<div class="banner-wrapper b2">
<h3>Watches</h3>
<a href="#">
<img alt="" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/banners/offer-banner2.jpg" /></a></div>
<div class="banner-wrapper b3">
<h3>Bags</h3>
<a href="#">
<img alt="" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/banners/offer-banner3_1.jpg" /></a></div>
</div>
<div class="banner-wrapper b4">
<h3>shop new</h3>
<h1>Collection</h1>
<a href="#">
<img alt="" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/banners/offer-banner4.jpg" /></a></div>
</div>
EOB
);

$blocks->setData($data_blocks)->save();

$data_blocks = array(
	'title'      => 'Footer Contact Info',
	'identifier' => 'footer_contact_info',
	'stores'     => $stores,
	'is_active'  => 1,
	'content'    => <<<EOB
			<div class="footer-contact-info">
<div><a href="#"><img alt="footer logo" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/logo-footer.png" /></a></div>
<address>
<div><i class="fa fa-map-marker">&nbsp;</i><span>123 Some Street, City, 12345 </span></div>
<div><i class="fa fa-phone">&nbsp;</i><span> +(408) 394-7557</span></div>
<div><i class="fa fa-envelope-o">&nbsp;</i><span> store@belvg.com</span></div>
</address>
<div class="social">
<ul class="link">
<li class="fb pull-left"><a title="Facebook" href="#" target="_blank" rel="nofollow">&nbsp;</a></li>
<li class="tw pull-left"><a title="Twitter" href="#" target="_blank" rel="nofollow">&nbsp;</a></li>
<li class="googleplus pull-left"><a title="GooglePlus" href="#" target="_blank" rel="nofollow">&nbsp;</a></li>
<li class="rss pull-left"><a title="RSS" href="#" target="_blank" rel="nofollow">&nbsp;</a></li>
<li class="pintrest pull-left"><a title="PInterest" href="#" target="_blank" rel="nofollow">&nbsp;</a></li>
<li class="linkedin pull-left"><a title="Linkedin" href="#" target="_blank" rel="nofollow">&nbsp;</a></li>
<li class="youtube pull-left"><a title="Youtube" href="#" target="_blank" rel="nofollow">&nbsp;</a></li>
</ul>
</div>
</div>
<div style="clear: both;">&nbsp;</div>
EOB
);

$blocks->setData($data_blocks)->save();

$data_blocks = array(
	'title'      => 'Footer Payment Logos',
	'identifier' => 'footer_payment_logos',
	'stores'     => $stores,
	'is_active'  => 1,
	'content'    => <<<EOB
			<div class="footer-payment-logos">
<div class="payment-logo-wrapper"><img alt="" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/banners/pay-logo1.png" /></div>
<div class="payment-logo-wrapper"><img alt="" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/banners/pay-logo2_1.png" /></div>
<div class="payment-logo-wrapper"><img alt="" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/banners/pay-logo3.png" /></div>
<div class="payment-logo-wrapper"><img alt="" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/banners/pay-logo4.png" /></div>
</div>
EOB
);

$blocks->setData($data_blocks)->save();

$data_blocks = array(
	'title'      => 'Brands Slider',
	'identifier' => 'brands_slider',
	'stores'     => $stores,
	'is_active'  => 1,
	'content'    => <<<EOB
			<h3>Our Brands</h3>
<div id="owl-brands">
<div class="item"><img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/brand1.png" /></div>
<div class="item"><img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/brand2_1.png" /></div>
<div class="item"><img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/brand3_1.png" /></div>
<div class="item"><img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/brand4.png" /></div>
<div class="item"><img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/brand5.png" /></div>
<div class="item"><img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/brand6.png" /></div>
<div class="item"><img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/brand7.png" /></div>
</div>
EOB
);

$blocks->setData($data_blocks)->save();

$data_blocks = array(
	'title'      => 'Footer Features',
	'identifier' => 'footer_features',
	'stores'     => $stores,
	'is_active'  => 1,
	'content'    => <<<EOB
			<div class="container">
<ul>
<li>
<div class="feature-box">
<div class="content"><i class="fa fa-plane">&nbsp;</i> Worldwide shipping</div>
</div>
</li>
<li>
<div class="feature-box">
<div class="content"><i class="fa fa-phone">&nbsp;</i> Need Help +1 800 123 1234</div>
</div>
</li>
<li>
<div class="feature-box">
<div class="content"><i class="fa fa-money">&nbsp;</i> Money Back Guarantee</div>
</div>
</li>
<li>
<div class="feature-box">
<div class="content"><i class="fa fa-briefcase">&nbsp;</i> 30 days return Service</div>
</div>
</li>
</ul>
</div>
EOB
);

$blocks->setData($data_blocks)->save();

$data_blocks = array(
	'title'      => 'Fullwidth Block',
	'identifier' => 'fullwidth_block',
	'stores'     => $stores,
	'is_active'  => 1,
	'content'    => <<<EOB
			<div class="block-parallax">
<div class="center-fix wow fadeInUp">
<h1 class="wow fadeInUp">Shop Accessories</h1>
<h4 class="wow fadeInUp delay-05s" style="margin-bottom: 50px;">An exclusive handbags by trending fashion designers</h4>
<a class="button border animated fadeInUp delay-1s" href="http://belvg.net/helen/magento_themes/theme3/index.php/bags.html">explore </a></div>
</div>
EOB
);

$blocks->setData($data_blocks)->save();

$data_blocks = array(
	'title'      => 'Header Links Block',
	'identifier' => 'header_links_text_block',
	'stores'     => $stores,
	'is_active'  => 1,
	'content'    => <<<EOB
			<div class="header-links-text-block"><span class="fa fa-star-o">&nbsp;</span>Explore &nbsp;<a href="#">Special Offers</a> &nbsp;every week!</div>
EOB
);

$blocks->setData($data_blocks)->save();

$data_blocks = array(
	'title'      => 'Sidebar Banner',
	'identifier' => 'sidebar_banner',
	'stores'     => $stores,
	'is_active'  => 1,
	'content'    => <<<EOB
			<div id="owl-sidebar-banner">
<div class="item">
<div class="sidebar-banner-content">
<h2>50% OFF</h2>
<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span><a class="sidebar-banner-link">Buy Now</a></div>
<img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/banners/slide5_4.jpg" /></div>
<div class="item">
<div class="sidebar-banner-content">
<h2>Summer Collection</h2>
<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span><a class="sidebar-banner-link">Buy Now</a></div>
<img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/banners/slide3.jpg" /></div>
<div class="item">
<div class="sidebar-banner-content">
<h2>Men's<br /> Collection</h2>
<span>Lorem ipsum dolor sit amet.</span><a class="sidebar-banner-link">Buy Now</a></div>
<img alt="Owl Image" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/banners/slide5_2.jpg" /></div>
</div>
<p><img alt="" src="http://belvg.net/helen/magento_themes/theme3/media/wysiwyg/banners/hot-trends-banner1.jpg" /></p>
EOB
);

$blocks->setData($data_blocks)->save();

$installer->endSetup();
