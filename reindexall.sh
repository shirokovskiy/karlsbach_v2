#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
# touch $THIS_DIR/maintenance.flag &
rm -f $THIS_DIR/var/locks/*.lock
DBNAME=karlsbach_v2
date

##ifStart=`date '+%d'`
# Make it once a month (first day of month)
##if [ $ifStart == 01 ]
##then
##    mysql --defaults-extra-file=$THIS_DIR/shell/tools/sql/mydbsql.cnf $DBNAME < $THIS_DIR/shell/magmi/import.cleaner.sql
##fi

php $THIS_DIR/shell/indexer.php reindexall

#mysql --defaults-extra-file=$THIS_DIR/shell/tools/sql/mydbsql.cnf $DBNAME < $THIS_DIR/shell/tools/sql/fix.rewrite.sql
#mysql --defaults-extra-file=$THIS_DIR/shell/tools/sql/mydbsql.cnf $DBNAME < $THIS_DIR/shell/tools/sql/fix.categories.sql
bash $THIS_DIR/rmcache.sh
date
rm -f $THIS_DIR/maintenance.flag &
rm -f $THIS_DIR/var/locks/*.lock
