<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 2/3/13
 * Time         : 7:12 PM
 * Description  :
 */
require_once "app/Mage.php";
umask(0);
Mage::app();

if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) {
    Mage::setIsDeveloperMode(true);
}

$file = 'orders.csv';
$local_file_path = BP.DS.'var'.DS.'import'.DS.$file;

$ftp_server = "146.66.199.228"; # "79.175.45.71"; // 'shishi-spb.ibsystems.ru'; 79.175.45.71
$ftp_user_name = 'ftpuser';
$ftp_user_pass = '1qaz2wsX';

/**
 * @var Phpwebstudio_ExportOrders_Model_Csv $exportCsv
 */
$exportCsv = Mage::getModel('exportorders/csv');
$exportCsv->checkCompleteOrders(); // эта команда установит заказы в статус Proceccing (из Pending)
$exportResult = $exportCsv->saveFile($local_file_path, array(1,5));

/**
 * Выложить заказы на FTP
 */
if ($exportResult /*&& !Mage::getIsDeveloperMode()*/) {
    $conn_id = ftp_connect($ftp_server);

    if ($conn_id) {
        $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

        if ($login_result) {
            if (ftp_put($conn_id, "new_orders/".$file, $local_file_path, FTP_ASCII)) {
                Mage::getModel('logger/mylog')->parse( "$file успешно загружен на сервер $ftp_server" );
            } else {
                Mage::getModel('logger/mylog')->parse( "Не удалось загрузить $file на сервер $ftp_server" );
            }
        } else {
            Mage::getModel('logger/mylog')->parse('Не могу авторизоваться на ' . $ftp_server . ' как user: ' . $ftp_user_name);
        }

        ftp_close($conn_id);
    } else {
        Mage::getModel('logger/mylog')->parse('Error FTP connection to ' . $ftp_server);
    }
}
