#!/usr/bin/env bash

# Detect ROOT user
AS_ROOT=0
if [[ $EUID -eq 0 ]]; then
   echo "This script has been run under root user "$EUID
   AS_ROOT=1
fi

# Detect execution path
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR

# Double check Magento folder
if [ ! -f mage ]
then
    echo "This is not root folder of Magento project"
    exit 1
fi

echo "Delete all old session files"
find $THIS_DIR/var/session/ -type f -mtime +1 -exec rm -f {} \;

echo "Clean cache files"
rm -rf $THIS_DIR/var/cache/* $THIS_DIR/var/report/*
rm -rf $THIS_DIR/media/tmp/*.*
rm -rf $THIS_DIR/media/css/*.css
rm -rf $THIS_DIR/media/js/*.js
find $THIS_DIR/media/catalog/product/cache/ -type f -mtime +14 -exec rm -f {} \;
find $THIS_DIR/var/backups/ -type f -name '*_db.gz' -mtime +110 -exec rm -f {} \;

echo "Clean logs"
cat /dev/null > $THIS_DIR/var/log/exception.log
cat /dev/null > $THIS_DIR/var/log/system.log

if [[ $AS_ROOT -eq 1 ]];
then
    echo "Restart Web services"
    if [ -d /var/lib/nginx_pagespeed ]
    then
        echo "Remove PageSpeed Cache"
        rm -rf /var/lib/nginx_pagespeed/*
    fi

    if [ -f /etc/init.d/nginx ]
    then
        /etc/init.d/nginx restart
    fi

    if [ -f /etc/init.d/php7.0-fpm ]
    then
	    /etc/init.d/php7.0-fpm restart
    fi

    if [ -f /etc/init.d/mysql ]
    then
        /etc/init.d/mysql restart
    fi

    if [ -f /etc/init.d/apache2 ]
    then
        /etc/init.d/apache2 restart
    fi
fi

rm -f $THIS_DIR/maintenance.flag &
