#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR

$THIS_DIR/shell/tools/getdump.sh
$THIS_DIR/shell/tools/restore.local.sh
