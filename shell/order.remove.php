<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 24.02.16
 * Time         : 13:20
 * Description  : Remove test orders by Order Number
 *
 * php ./shell/order.remove.php 100000005 [remove]
 */
set_time_limit(0);
date_default_timezone_set('Europe/Moscow');
error_reporting(E_ALL & ~E_NOTICE | E_STRICT);
umask(0);
ini_set('memory_limit', '1024M');

$mageFilename = dirname(__FILE__)."/../app/Mage.php";

require_once $mageFilename;

umask(0);

Mage::app('admin')->setUseSessionInUrl(false);

$order_ids = [];
$do_remove = false;

if (isset($argv[1]) && !empty($argv[1])) {
    $order_ids = $argv[1]; // use 100000123 | can use few orders with comma 100000123,100000124,100000125
    if (preg_match("/,/",$order_ids)) {
        $order_ids = explode(',',$order_ids);
    } else {
        $order_ids = [$order_ids];
    }
}

if (isset($argv[2]) && !empty($argv[2])) {
    if ($argv[2] == 'remove') {
        $do_remove = true;
    }
}

#die("Be careful".PHP_EOL);

if (is_array($order_ids) && !empty($order_ids)) {
    $collection = Mage::getModel('sales/order')->getCollection();
    foreach ($collection as $data) {
        $id = $data['increment_id'];

        if (in_array($id, $order_ids)) {
            try{
                if ($do_remove) {
                    Mage::getModel('sales/order')->loadByIncrementId($id)->delete();
                    echo "order #".$id." is removed".PHP_EOL;
                } else {
                    // close order or remove
                    $order = Mage::getModel('sales/order')->loadByIncrementId($id);
                    #$order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true, 'Счёт отменён вручную.');
                    $order->setState(Mage_Sales_Model_Order::STATE_NEW, true, 'Заказ вернули в состояние ожидания.');
                    $order->setStatus("pending");
                    $order->save();
                }
            }catch(Exception $e){
                echo "order #".$id." could not be removed: ".$e->getMessage().PHP_EOL;
            }
        }
    }
} else {
    echo "no orders IDs".PHP_EOL;
}

echo "complete.".PHP_EOL;
