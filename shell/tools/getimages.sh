#!/usr/bin/env bash
date
echo "Start getting images!"
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd "../.."
RF="$(pwd)"

echo "Rsync SKIN"
# u = update
# a = archive data
# r = recursive searh
# p = preserve permissions
# v = verbose
# P = progress to show
# h = human readable data (Mb, etc)

rsync -Lpgouar kb:~/mg/skin $RF/

echo "Rsync MEDIA folders..."

if [ ! -d $RF/media ]
then
    echo "Create folder /media"
    mkdir -p $RF/media
fi

cd $RF/media/

if [ -d $RF/media ]
then
    echo "Copy media folders..."
    rsync -Phuvar --exclude=cache --exclude=tmp --size-only kb:~/www/v2/media/wysiwyg .

    TIME_LIMIT=15
    choice=""
    echo "Are you sure you want to download PRODUCT images? <y/n>"
    read -t $TIME_LIMIT choice

    if [ ! -z "$choice" ]
    then
	case "$choice" in
          y|Y ) rsync -Phupvar --exclude=cache --exclude=tmp --size-only kb:~/www/v2/media/catalog .;;
          n|N ) echo "Product images download skip!";;
          * ) echo "invalid choice :( no RSYNCing";;
        esac
    else
        echo "No choice to download product images"
    fi
fi

