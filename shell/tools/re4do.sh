#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR

$THIS_DIR/getdump.sh
$THIS_DIR/restore.db4docker.sh
