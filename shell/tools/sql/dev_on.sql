--
SET FOREIGN_KEY_CHECKS = 0;

UPDATE `core_config_data` SET `value` = '1' WHERE `path` LIKE 'dev/log/active';
UPDATE `core_config_data` SET `value` = '1' WHERE `path` LIKE 'dev/debug/template_hints';
UPDATE `core_config_data` SET `value` = '1' WHERE `path` LIKE 'dev/debug/template_hints_blocks';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` LIKE 'dev/css/merge_css_files';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` LIKE 'dev/js/merge_files';
UPDATE `core_config_data` SET `value` = '1' WHERE `path` LIKE 'dev/debug/profiler';

SET FOREIGN_KEY_CHECKS = 1;
