UPDATE `core_config_data` SET `value` = 'http://karlsbach.test/' WHERE `path` LIKE 'web/unsecure/base_url';
UPDATE `core_config_data` SET `value` = 'http://karlsbach.test/' WHERE `path` LIKE 'web/secure/base_url';
TRUNCATE TABLE `log_url`;
TRUNCATE TABLE `log_url_info`;
TRUNCATE TABLE `log_visitor`;
TRUNCATE TABLE `log_visitor_info`;
TRUNCATE TABLE `log_visitor_online`;
