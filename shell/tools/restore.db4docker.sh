#!/usr/bin/env bash
date
DBUSER=myadmin
DBNAME=karlsbach_v2
DOCKERDB=database
CUR_DATE=$(date +%Y-%m-%d---%H-%M)
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd '../..'
KB_DIR="$(pwd)"
cd $THIS_DIR
BKP_DIR=$KB_DIR/var/backups
BKP_FILE=$DBNAME.local-dump.$CUR_DATE.sql
DBFILE=$DBNAME.sql
FILENAME=$DBFILE.7z
DESTINATION=$BKP_DIR/$DBFILE
DESTINATION_ZIP=$BKP_DIR/$FILENAME
DESTINATION_BKP=$BKP_DIR/$BKP_FILE

if [ -f $DESTINATION_ZIP ]
then
    cd $BKP_DIR
    echo "Start unzip: $DESTINATION_ZIP"
    7z e $DESTINATION_ZIP -so > $BKP_DIR/$DBFILE
else
    echo "There is no downloaded file: $DESTINATION_ZIP"
fi

echo "Make local database backup"
mysqldump --defaults-extra-file=$THIS_DIR/sql/docker-mysql.cnf --opt $DOCKERDB > $DESTINATION_BKP

if [ -f $DESTINATION ]
then
    echo "Run DB restore process"
    mysql --defaults-extra-file=$THIS_DIR/sql/docker-mysql.cnf $DOCKERDB < $DESTINATION
else
    echo "There is NO remote dump file: $DESTINATION"
    exit 1
fi

echo "Run DB updates"
mysql --defaults-extra-file=$THIS_DIR/sql/docker-mysql.cnf $DOCKERDB < $THIS_DIR/sql/remote.dump.helper.sql
mv -f $DESTINATION $DESTINATION.just.restored.bkp.$CUR_DATE.sql

bash $KB_DIR/rmcache.sh
date
