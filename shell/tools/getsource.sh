#!/usr/bin/env bash
#
#
# @project  Karlsbach
# @author   Dmitriy Shirokovskiy
#
date
echo "Start getting source!"
CUR_DATE=$(date +%Y-%m-%d---%H-%M)
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
ROOT_DIR="../.."

echo "Download sources.../"
echo "code"
rsync -Phuvar --size-only kb:~/www/v2/app/code $ROOT_DIR/app/
echo "design"
rsync -Phuvar --size-only kb:~/www/v2/app/design $ROOT_DIR/app/
echo "locale"
rsync -Phuvar --size-only kb:~/www/v2/app/locale $ROOT_DIR/app/
echo "js"
rsync -Phuvar --size-only kb:~/www/v2/js $ROOT_DIR/
echo "lib"
rsync -Phuvar --size-only kb:~/www/v2/lib $ROOT_DIR/
echo "skin"
rsync -Phuvar --size-only kb:~/www/v2/skin $ROOT_DIR/

