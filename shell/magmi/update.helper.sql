-- magento 1.7
SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE catalog_product_flat_1;
-- TRUNCATE catalog_product_flat_2;
-- TRUNCATE catalog_product_flat_3;
-- TRUNCATE catalog_product_flat_4;
-- TRUNCATE catalog_product_flat_5;
-- TRUNCATE catalog_product_flat_6;
-- TRUNCATE catalog_product_flat_7;

SET FOREIGN_KEY_CHECKS = 1;
