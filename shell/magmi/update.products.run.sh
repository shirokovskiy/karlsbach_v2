#!/usr/bin/env bash
#
# This script will start MAGMI process to update existing products in Magento
# mode=...
# update : will skip non existing skus & update existing ones
# create : will create new products for non exisiting skus, will update existing ones
# xcreate : will create new products for non exisiting skus, will skip existing ones
date
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd '../..'
DR="$(pwd)"
cd $THIS_DIR
FILE=update_products.csv
PROF=KarlsbachUpd
CUR_DATE=$(date +%Y-%m-%d---%H-%M)
DBNAME=karlsbach_v2

if [ -f $DR/var/import/$FILE ]
then
    echo "Start UPDATE PRODUCTS"
    cd $DR
    mysql --defaults-extra-file=$DR/shell/tools/sql/mydbsql.cnf $DBNAME < $THIS_DIR/update.helper.sql

    php ./magmi.importer/cli/magmi.cli.php -profile=$PROF -mode=update
    mv $DR/var/import/$FILE $DR/var/import/update_products.$CUR_DATE.done.csv

    echo "Backup MagMI Progress Log"
    cp $DR/magmi.importer/state/progress.txt $DR/var/log/update.progress.$(date +%Y.%m.%d__%H.%M).txt

    find $DR/var/import/ -type f -name "update_products.20*" -mtime +31 -exec rm -f {} \;

#    mail -s "Update Karlsbach Products" jimmy.webstudio@gmail.com
else
    echo "No "$FILE" at "$(date "+%F %H:%M")
fi
echo "The End"
date
