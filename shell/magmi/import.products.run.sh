#!/usr/bin/env bash
#
# This file will start MAGMI import process
# mode=...
# update : will skip non existing skus & update existing ones
# create : will create new products for non exisiting skus, will update existing ones
# xcreate : will create new products for non exisiting skus, will skip existing ones
date
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd '../..'
DR="$(pwd)"
cd $THIS_DIR
MSHELL=$DR/shell/magmi
DBNAME=karlsbach_v2

if [ -f $DR/var/import/import_products.csv ]
then
    echo "Close WebShop!*!*!"
    touch $DR/maintenance.flag

    echo "Start SQL-Helper"
    mysql --defaults-extra-file=$DR/shell/tools/sql/mydbsql.cnf $DBNAME < $MSHELL/import.helper.sql
    mysql --defaults-extra-file=$DR/shell/tools/sql/mydbsql.cnf $DBNAME < $MSHELL/import.helper.2.sql

    bash $DR/reindexall.sh

    cd $DR

    echo "Start IMPORT PRODUCTS"
    php $DR/magmi.importer/cli/magmi.cli.php -mode=create

    echo "Backup IMPORT PRODUCTS CSV-file"
    mv -f $DR/var/import/import_products.csv $DR/var/import/import_products.$(date +%Y-%m-%d__%H.%M).csv

    echo "Backup MagMI Progress Log"
    cp $DR/magmi.importer/state/progress.txt $DR/var/log/import.progress.$(date +%Y.%m.%d__%H.%M).txt

    echo "Remove old files of imports & logs"
    find $DR/var/import/ -type f -name "import_products.20*" -mtime +92 -exec rm -f {} \;
    find $DR/var/log/ -type f -name "import.progress.20*" -mtime +31 -exec rm -f {} \;
    find $DR/var/log/ -type f -name "*_email_*" -mtime +31 -exec rm -f {} \;

    bash $DR/reindexall.sh

    if [ -f $DR/maintenance.flag ]
    then
        echo "Open WebShop !!!"
        rm -f $DR/maintenance.flag
    fi
else
    echo "No import_products.csv at "$(date "+%F %H:%M")
fi

echo "The End"
date
