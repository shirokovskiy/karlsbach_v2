-- magento 1.7
SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE core_url_rewrite;
-- TRUNCATE catalog_product_flat_1;
-- TRUNCATE catalog_product_flat_2;
-- TRUNCATE catalog_product_flat_3;
-- TRUNCATE catalog_product_flat_4;
-- TRUNCATE catalog_product_flat_5;
-- TRUNCATE catalog_product_flat_6;
-- TRUNCATE catalog_product_flat_7;





UPDATE cataloginventory_stock_status_idx SET `qty` = 0, `stock_status` = 0 WHERE website_id IN (1,4,5,7); -- Only Russian Websites
UPDATE cataloginventory_stock_status SET `qty` = 0 WHERE website_id IN (1,4,5,7); -- Only Russian Websites
UPDATE cataloginventory_stock_status SET `stock_status` = 0 WHERE website_id IN (1,4,5,7); -- Only Russian Websites
-- UPDATE catalog_product_entity_int SET `value` = 3 WHERE store_id IN (1,4,5,7) AND attribute_id = 102; -- Only Russian Websites
SELECT @status_attr := `attribute_id` FROM `eav_attribute` WHERE `attribute_code` = 'status' LIMIT 1;
UPDATE catalog_product_entity_int SET `value` = 2 WHERE store_id IN (1,4,5,7) AND attribute_id = @status_attr; -- value = 2 means Disabled


-- UPDATE `cataloginventory_stock_item` SET `qty` = 0, `is_in_stock` = 0  WHERE `product_id` IN (SELECT `product_id` FROM `cataloginventory_stock_status` WHERE `website_id` IN (1,4,5,7));





DELETE FROM `catalog_category_product_index` WHERE `store_id` IN (1,4,5,7) AND `visibility` = 4 AND `category_id` NOT IN (139,140,141,142,143,144,156,157,158,159,160,161,162,163,164,165,166,167,168);
-- DELETE FROM `catalog_product_entity_decimal` WHERE `store_id` = 0; -- Удалить общие цены
-- DELETE FROM `catalog_product_entity_datetime` WHERE `store_id` = 0;
-- DELETE FROM `catalog_product_entity_int` WHERE `store_id` = 0;
-- DELETE FROM `catalog_product_entity_media_gallery_value` WHERE `store_id` = 0;
-- DELETE FROM `catalog_product_entity_text` WHERE `store_id` = 0;
-- DELETE FROM `catalog_product_entity_varchar` WHERE `store_id` = 0;

SET FOREIGN_KEY_CHECKS = 1;
