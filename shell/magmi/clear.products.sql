-- Use this file only when you need to clean all products in database
-- @author	Dmitry Shirokovskiy, www.phpwebstudio.com
TRUNCATE cataloginventory_stock_item;
TRUNCATE cataloginventory_stock_status;
TRUNCATE cataloginventory_stock_status_idx;
TRUNCATE catalogsearch_fulltext;
-- TRUNCATE catalog_category_product;
TRUNCATE catalog_category_product_index;
TRUNCATE catalog_product_index_price_idx;
DELETE FROM catalog_product_entity; -- необходимо зайти и удалить нужные записи
ALTER TABLE catalog_product_entity AUTO_INCREMENT=1; -- затем выставить новый AUTO_INCREMENT
TRUNCATE catalog_product_entity_datetime; -- таблица очиститься сама, выставить новый AUTO_INCREMENT
TRUNCATE catalog_product_entity_decimal; -- таблица очиститься сама, выставить новый AUTO_INCREMENT
TRUNCATE catalog_product_entity_int; -- таблица очиститься сама, выставить новый AUTO_INCREMENT
ALTER TABLE catalog_product_entity_media_gallery AUTO_INCREMENT=1; -- выставить новый AUTO_INCREMENT
TRUNCATE catalog_product_entity_text; -- таблица очиститься сама, выставить новый AUTO_INCREMENT
TRUNCATE catalog_product_entity_varchar; -- таблица очиститься сама, выставить новый AUTO_INCREMENT
TRUNCATE catalog_product_super_link; -- таблица очиститься сама, выставить новый AUTO_INCREMENT
TRUNCATE importexport_importdata; -- таблица очиститься сама, выставить новый AUTO_INCREMENT. Данная таблица вообще видимо только при импорте нужна.
TRUNCATE core_url_rewrite; -- Также, необходимо заглянуть в таблицу core_url_rewrite на те записи где product_id IS NOT NULL
